from django.contrib import admin
from django.contrib.admin.sites import AdminSite

from .models import DeclaraciónJurada, Estudiante, Ingreso


class VueltaSeguraAdminSite(AdminSite):
   site_header = 'Vuelta Segura - Establecimiento'
   site_title = 'Vuelta Segura - Establecimiento'
   index_title = 'Administración de ingresos'
   site_url = None


class IngresoEscuelaAdmin(admin.ModelAdmin):
   list_display = ("estudiante_nombre_completo", "estudiante_curso", "estado", "fecha")
   date_hierarchy = "fecha" # Probar cargando ingresos distintos días
   list_select_related = ("estudiante", "ddjj")
   fieldsets = (
      ("Declaración jurada", {
         'fields': (
            'ddjj_temperatura',
            'ddjj_perdida_olfato',
            'ddjj_perdida_gusto',
            'ddjj_tos',
            'ddjj_dolor_garganta',
            'ddjj_dificultad_respiratoria',
            'ddjj_cefalea',
            'ddjj_mialgias',
            'ddjj_diarreas',
            'ddjj_vómitos',
            'ddjj_persona_sospechosa_covid_19',
            'ddjj_viaje_zona_aspo_últimos_14_dias',
            'ddjj_cáncer',
            'ddjj_diabetes',
            'ddjj_enf_hepática',
            'ddjj_enf_renal_crónica',
            'ddjj_enf_respiratoria',
            'ddjj_enf_cardiologica',
            'ddjj_condición_defensas_bajas',
            'ddjj_nombre_rp'
            'ddjj_dni_rp'
         )
      }),
   )

   @admin.display(description="estudiante")
   def estudiante_nombre_completo(self, obj):
      return "{0} {1}".format(obj.estudiante.nombre, obj.estudiante.apellido)

   @admin.display(description="curso")
   def estudiante_curso(self, obj):
      return obj.estudiante.curso

   @admin.display(description="temperatura corporal")
   def ddjj_temperatura(self, obj):
      return obj.ddjj.temperatura

   @admin.display(description="marcada pérdida de olfato de manera repentina")
   def ddjj_perdida_olfato(self, obj):
      return obj.ddjj.perdida_olfato

   @admin.display(description="marcada pérdida del gusto de manera repentina")
   def ddjj_perdida_gusto(self, obj):
      return obj.ddjj.perdida_gusto

   @admin.display(description="tos")
   def ddjj_tos(self, obj):
      return obj.ddjj.tos

   @admin.display(description="dolor de garganta")
   def ddjj_dolor_garganta(self, obj):
      return obj.ddjj.dolor_garganta

   @admin.display(description="dificultad respiratoria o falta de aire")
   def ddjj_dificultad_respiratoria(self, obj):
      return obj.ddjj.dificultad_respiratoria

   @admin.display(description="cefalea")
   def ddjj_cefalea(self, obj):
      return obj.ddjj.cefalea

   @admin.display(description="mialgias")
   def ddjj_mialgias(self, obj):
      return obj.ddjj.mialgias

   @admin.display(description="diarreas")
   def ddjj_diarreas(self, obj):
      return obj.ddjj.diarreas

   @admin.display(description="vómitos")
   def ddjj_vómitos(self, obj):
      return obj.ddjj.vómitos

   @admin.display(description="Convive con alguna persona que es caso sospechoso o confirmado COVID-19")
   def ddjj_persona_sospechosa_covid_19(self, obj):
      return "Sí" if obj.ddjj.persona_sospechosa_covid_19 else "No"

   @admin.display(description="Convive con alguna persona que viajó a zona de ASPO en los últimos 14 días")
   def ddjj_viaje_zona_aspo_últimos_14_dias(self, obj):
      return "Sí" if obj.ddjj.viaje_zona_aspo_últimos_14_dias else "No"

   @admin.display(description="tiene o tuvo cáncer")
   def ddjj_cáncer(self, obj):
      return "Sí" if obj.ddjj.cáncer else "No"

   @admin.display(description="tiene diabetes")
   def ddjj_diabetes(self, obj):
      return "Sí" if obj.ddjj.diabetes else "No"

   @admin.display(description="tiene alguna enfermedad hepática")
   def ddjj_enf_hepática(self, obj):
      return "Sí" if obj.ddjj.enf_hepática else "No"

   @admin.display(description="tiene enfermedad renal crónica")
   def ddjj_enf_renal_crónica(self, obj):
      return "Sí" if obj.ddjj.enf_renal_crónica else "No"

   @admin.display(description="tiene enfermedad respiratoria")
   def ddjj_enf_respiratoria(self, obj):
      return "Sí" if obj.ddjj.enf_respiratoria else "No"

   @admin.display(description="tiene enfermedad cardiológica")
   def ddjj_enf_cardiologica(self, obj):
      return "Sí" if obj.ddjj.enf_cardiologica else "No"

   @admin.display(description="tiene alguna condición que baja las defensas")
   def ddjj_condición_defensas_bajas(self, obj):
      return "Sí" if obj.ddjj.condición_defensas_bajas else "No"

   @admin.display(description="Nombre del responsable parental")
   def ddjj_nombre_rp(self, obj):
      return "Sí" if obj.ddjj.nombre_rp else "No"

   @admin.display(description="DNI del responsable parental")
   def ddjj_dni_rp(self, obj):
      return "Sí" if obj.ddjj.dni_rp else "No"


# Configuración del admin del establecimiento
admin_escuela = VueltaSeguraAdminSite(name='vueltaseguraadmin')
admin_escuela.register(Ingreso, IngresoEscuelaAdmin)

# Configuración del admin general
admin.site.register(Estudiante)
admin.site.register(DeclaraciónJurada)
admin.site.register(Ingreso)