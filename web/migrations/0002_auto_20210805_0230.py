# Generated by Django 3.2.5 on 2021-08-05 02:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='cefalea',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='diarreas',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='dificultad_respiratoria',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='dolor_garganta',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='mialgias',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='perdida_gusto',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='perdida_olfato',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='tos',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='declaraciónjurada',
            name='vómitos',
            field=models.BooleanField(default=False),
        ),
    ]
