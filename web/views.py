import json

from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from .models import DeclaraciónJurada, Estudiante, Ingreso
from django.http import JsonResponse

# Create your views here.
@csrf_exempt
def ingresos(request):
    if request.method == "POST":
        datos_request = request.body.decode("utf-8")
        datos = json.loads(datos_request)

        # 1 buscar el estudiante por el id. Si no existe, crearlo.
        datos_estudiante = datos["estudiante"]
        try: 
            estudiante = Estudiante.objects.get(pk=datos_estudiante["id"])
        except Estudiante.DoesNotExist:
            estudiante = Estudiante(**datos_estudiante)
            estudiante.save()

        # 2 buscar la ddjj por id. Si no existe, crearla.
        datos_ddjj = datos["ddjj"]
        try: 
            ddjj = DeclaraciónJurada.objects.get(pk=datos_ddjj["id"])
        except DeclaraciónJurada.DoesNotExist:
            ddjj = DeclaraciónJurada(estudiante=estudiante, **datos_ddjj)
            ddjj.save()

        # 3 crear el ingreso con la informacion obtenida mas el estudiante mas la ddjj.
        ingreso = Ingreso(estudiante=estudiante, ddjj=ddjj, estado=datos["ingreso"]["estado"])
        ingreso.save()
        
        datos["ingreso"]["fecha"] = ingreso.fecha.strftime("%d/%m/%Y %H:%M:%S")
        datos["ingreso"]["id"] = ingreso.id

        respuesta = JsonResponse(datos, safe=False) 
        respuesta.status_code = 201

        return respuesta
