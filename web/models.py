import uuid

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


# Create your models here.
class Estudiante(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    curso = models.CharField(max_length=20)
    dni = models.CharField(max_length=8)
    
    def __str__(self):
        return "Estudiante: {0} {1} | Curso: {2}".format(
            self.nombre,
            self.apellido,
            self.curso
        )


class DeclaraciónJurada(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    fecha = models.DateField()
    estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
    temperatura = models.FloatField(default=37, validators=[MinValueValidator(35), MaxValueValidator(40)])
    perdida_olfato = models.BooleanField(default=False)
    perdida_gusto = models.BooleanField(default=False)
    tos = models.BooleanField(default=False)
    dolor_garganta = models.BooleanField(default=False)
    dificultad_respiratoria = models.BooleanField(default=False)
    cefalea = models.BooleanField(default=False)
    mialgias = models.BooleanField(default=False)
    diarreas = models.BooleanField(default=False)
    vómitos = models.BooleanField(default=False)
    persona_sospechosa_covid_19 = models.BooleanField(default=False)
    viaje_zona_aspo_últimos_14_dias = models.BooleanField(default=False)
    cáncer = models.BooleanField(default=False)
    diabetes = models.BooleanField(default=False)
    enf_hepática = models.BooleanField(default=False)
    enf_renal_crónica = models.BooleanField(default=False)
    enf_respiratoria = models.BooleanField(default=False)
    enf_cardiologica = models.BooleanField(default=False)
    condición_defensas_bajas = models.BooleanField(default=False)
    nombre_rp = models.CharField(max_length=40)
    dni_rp = models.CharField(max_length=9)

    def __str__(self):
        return "Estudiante: {0} {1} | Curso: {2} | DDJJ: {3}".format(
            self.estudiante.nombre,
            self.estudiante.apellido,
            self.estudiante.curso,
            self.fecha.strftime("%d/%m/%Y")
        )

    class Meta:
        verbose_name_plural = "Declaraciones Juradas"


class Ingreso(models.Model):

    estados_ingreso = [
        ("Permitido", "Permitido"),
        ("Denegado", "Denegado"),
        ("Permitido c/observaciones", "Permitido c/observaciones")
    ]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True) 
    estado = models.CharField(
        choices=estados_ingreso, max_length=25, default="Permitido"
    )
    ddjj = models.ForeignKey(DeclaraciónJurada, on_delete=models.RESTRICT)

    def __str__(self):
        return "Estudiante: {0} {1} | Curso: {2} | Ingreso: {3} | Fecha: {4}".format(
            self.estudiante.nombre,
            self.estudiante.apellido,
            self.estudiante.curso,
            self.estado,
            self.fecha.strftime("%d/%m/%Y %H:%M:%S")
        )