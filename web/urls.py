from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from . import views

app_name = "web"

urlpatterns = [
    path("ingresos/", views.ingresos, name="ingresos"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)